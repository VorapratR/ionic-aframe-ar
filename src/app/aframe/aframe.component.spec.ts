import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AframeComponent } from './aframe.component';

describe('AframeComponent', () => {
  let component: AframeComponent;
  let fixture: ComponentFixture<AframeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AframeComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
